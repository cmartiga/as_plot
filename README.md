# AS-plot

AS-plot is a small library for easy plotting differen types of alternative splicing events using matplotlib framework. This allows integrating AS types images in regular python plots for AS data analysis

## Installation

Download the repository using git and cd into it

```bash
git clone git@bitbucket.org:cmartiga/AS_plot.git
```

Install using setuptools
```bash
cd AS_plot
python setup.py install
```

# Usage

AS-plot can be easily used to plot custom alternative splicing events of arbitrary complexity. Check the [test](https://bitbucket.org/cmartiga/as_plot/src/master/test/tests_event_plots.py) for how to plot general exmaples.

We first need to define the region of the matplotlib axes object in which the AS event will be drawn

```python
from AS_plot import ASeventPlot

event = ASeventPlot(axes=axes, pos=(-10, 20), height=4, width=20)
```

Add different exons

```python
exon1 = event.add_exon(xstart=0, width=4, alternative=False)
exon2 = event.add_exon(xstart=10, width=4, alternative=True)
exon3 = event.add_exon(xstart=20, width=4, alternative=False)
exon4 = event.add_exon(xstart=30, width=4, alternative=False)
```

Conect them through splice junctions at different positions (top or bottom)

```python
event.plot_sj(exon1, exon2, loc='top')
event.plot_sj(exon2, exon3, loc='top')
event.plot_sj(exon1, exon3, loc='bottom')
```

...or through unspliced introns


```python
event.plot_intron(exon3, exon4)
event.plot_intron(None, exon1)
```

If we want to show only the AS event, we can remove the axis from the plot

```python
event.clean_axis()
```

Resulting in the final plot

![Arbitrary gene splicing patterns](https://bitbucket.org/cmartiga/as_plot/raw/master/plots/as_event.png)

There are also specific built-in methods to plot the 5 main types of AS events

```python
event.plot_exon_skipping()
event.plot_alternative_donor()
event.plot_alternative_acceptor()
event.plot_intron_retention()
event.plot_mutually_exclusive_exons()
```

![Alternative splicing events](https://bitbucket.org/cmartiga/as_plot/raw/master/plots/as_types.png)

These plots can be included in regular matplotlib figures

![Alternative splicing events](https://bitbucket.org/cmartiga/as_plot/raw/master/plots/as_plot.png)
