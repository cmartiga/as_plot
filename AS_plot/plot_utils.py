#!/usr/bin/env python
import matplotlib.pyplot as plt
import seaborn as sns


def init_fig(nrow=1, ncol=1, figsize=None, style='white',
             colsize=3, rowsize=3):
    sns.set_style(style)
    if figsize is None:
        figsize = (colsize * ncol, rowsize * nrow)
    fig, axes = plt.subplots(nrow, ncol, figsize=figsize)
    return(fig, axes)


def savefig(fig, fpath, tight=True):
    if tight:
        fig.tight_layout()
    fig.savefig(fpath, format=fpath.split('.')[-1], dpi=180)
    plt.close()
