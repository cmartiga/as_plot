from os.path import join, abspath, dirname

# Package version
VERSION = '0.1.0'

# Directories
BASE_DIR = abspath(join(dirname(__file__)))
PLOTS_DIR = join(BASE_DIR, '..', 'plots')

# Event types
ES = 'exon_skipping'
IR = 'intron_retention'
AD = 'alternative_donor'
AA = 'alternative_acceptor'
MXE = 'mutually_exclusive'
RMATS_TYPES = [ES, IR, AD, AA, MXE]
