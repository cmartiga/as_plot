#!/usr/bin/env python
import numpy as np
import seaborn as sns
from matplotlib.patches import Rectangle


class Exon(object):
    def __init__(self, xstart, ystart, width, height, alternative,
                 exon_color, alternative_color, linecolor, lw):
        self.color = alternative_color if alternative else exon_color
        self.xstart = xstart
        self.ystart = ystart
        self.width = width
        self.height = height
        self.xend = xstart + width
        self.yend = ystart + height
        self.linecolor = linecolor
        self.lw = lw
    
    def plot(self, axes):
        rect = Rectangle((self.xstart, self.ystart), width=self.width,
                         height=self.height, linewidth=self.lw,
                         edgecolor=self.linecolor, facecolor=self.color)
        axes.add_patch(rect)


class ASeventPlot(object):
    def __init__(self, axes, pos=(0, 0), height=4, width=30, exon_prop=0.4,
                 lw=1, exon_color='darkorange', alternative_color='beige', 
                 linecolor='black'):
        self.axes = axes
        self.xstart, self.ystart = pos
        self.yend = self.ystart + height
        self.height = height
        self.width = width
        self.xend = self.xstart + width
        self.exon_height = exon_prop * height
        self.intron_y = self.ystart + 0.5 * height
        
        self.exon_ystart = self.ystart + self.height * (1 - exon_prop) / 2
        self.exon_yend = self.exon_ystart + self.exon_height
        
        self.exon_color = exon_color
        self.alternative_color = alternative_color
        self.lw = lw
        self.linecolor = linecolor
        
        self.exon_width = 3
        self.intron_width = 3
        self.alt_ss_width = 1.5
    
    def add_exon(self, xstart, width=None, alternative=False):
        if width is None:
            width = self.exon_width
        exon = Exon(xstart, self.exon_ystart, width, self.exon_height,
                    alternative, self.exon_color, self.alternative_color,
                    self.linecolor, self.lw)
        exon.plot(self.axes)
        return(exon)
    
    def plot_sj(self, exon1, exon2, loc='top'):
        xstart = exon1.xend
        xend = exon2.xstart
        xmid = xstart + (xend - xstart) / 2.
        
        if loc == 'top':
            ystart = self.exon_yend
            yend = self.yend
        elif loc == 'bottom':
            ystart = self.exon_ystart
            yend = self.ystart
        else:
            raise ValueError()
        
        self.axes.plot((xstart, xmid, xend), (ystart, yend, ystart),
                       lw=self.lw, color=self.linecolor)
    
    def plot_intron(self, exon1, exon2):
        if exon1 is None:
            xstart = exon2.xstart - exon2.width
        else:
            xstart = exon1.xend
            
        if exon2 is None:    
            xend = exon1.end + exon1.width
        else:
            xend = exon2.xstart
            
        self.axes.plot((xstart, xend), (self.intron_y, self.intron_y),
                       lw=self.lw, color=self.linecolor)
    
    def clean_axis(self):
        sns.despine(ax=self.axes, bottom=True, left=True)
        self.axes.set_xticklabels([])
        self.axes.set_yticklabels([])
        self.axes.set_xlabel('')
        self.axes.set_ylabel('')
        self.axes.set_ylim((self.ystart * 0.95, self.yend * 1.05))
    
    def plot_exon_skipping(self):
        width = self.width / 5.
        xs = np.linspace(self.xstart, self.xend, 6)
        
        exon1 = self.add_exon(xstart=xs[0], width=width, alternative=False)
        exon2 = self.add_exon(xstart=xs[2], width=width, alternative=True)
        exon3 = self.add_exon(xstart=xs[4], width=width, alternative=False)
        
        self.plot_sj(exon1, exon2, loc='top')
        self.plot_sj(exon2, exon3, loc='top')
        self.plot_sj(exon1, exon3, loc='bottom')
    
    def calc_xs_widths(self, widths):
        widths = widths / widths.sum() * self.width
        xs = np.append(self.xstart, np.cumsum(widths) + self.xstart)
        return(xs, widths)
    
    def plot_alternative_donor(self):
        widths = np.array([self.exon_width, self.alt_ss_width, 
                           self.intron_width, self.exon_width])
        xs, widths = self.calc_xs_widths(widths)
        
        exon1 = self.add_exon(xstart=xs[0], width=widths[0], alternative=False)
        exon2 = self.add_exon(xstart=xs[1], width=widths[1], alternative=True)
        exon3 = self.add_exon(xstart=xs[3], width=widths[3], alternative=False)
        
        self.plot_sj(exon1, exon3, loc='top')
        self.plot_sj(exon2, exon3, loc='bottom')
    
    def plot_alternative_acceptor(self):
        widths = np.array([self.exon_width, self.intron_width, 
                           self.alt_ss_width, self.exon_width])
        xs, widths = self.calc_xs_widths(widths)
        
        exon1 = self.add_exon(xstart=xs[0], width=widths[0], alternative=False)
        exon2 = self.add_exon(xstart=xs[2], width=widths[2], alternative=True)
        exon3 = self.add_exon(xstart=xs[3], width=widths[3], alternative=False)
        
        self.plot_sj(exon1, exon2, loc='top')
        self.plot_sj(exon1, exon3, loc='bottom')
    
    def plot_intron_retention(self):
        widths = np.array([self.exon_width, 1.5 * self.intron_width, 
                           self.exon_width])
        xs, widths = self.calc_xs_widths(widths)
        
        exon1 = self.add_exon(xstart=xs[0], width=widths[0], alternative=False)
        exon2 = self.add_exon(xstart=xs[2], width=widths[2], alternative=False)
        
        self.plot_sj(exon1, exon2, loc='top')
        self.plot_intron(exon1, exon2)
    
    def plot_mutually_exclusive_exons(self):
        width = self.width / 7.
        xs = np.linspace(self.xstart, self.xend, 8)
        
        exon1 = self.add_exon(xstart=xs[0], width=width, alternative=False)
        exon2 = self.add_exon(xstart=xs[2], width=width, alternative=True)
        exon3 = self.add_exon(xstart=xs[4], width=width, alternative=True)
        exon4 = self.add_exon(xstart=xs[6], width=width, alternative=False)
        
        self.plot_sj(exon1, exon2, loc='top')
        self.plot_sj(exon2, exon4, loc='top')
        self.plot_sj(exon1, exon3, loc='bottom')
        self.plot_sj(exon3, exon4, loc='bottom')
