#!/usr/bin/env python
from setuptools import setup, find_packages
from AS_quant.settings import VERSION


def main():
    description = 'Library for plotting Alternative Splicing events'
    setup(
        name='AS_plot',
        version=VERSION,
        description=description,
        author_email='cmarti@cnic.es',
        url='https://bitbucket.org/cmartiga/AS_plot',
        packages=find_packages(),
        include_package_data=True,
        entry_points={
            'console_scripts': []},
        install_requires=['numpy', 'seaborn', 'matplotlib'],
        platforms='ALL',
        keywords=['bioinformatics', 'alternative splicing', 'plots'],
        classifiers=[
            "Programming Language :: Python :: 3",
            'Intended Audience :: Science/Research',
            "License :: OSI Approved :: MIT License",
            "Operating System :: OS Independent",
        ],
    )
    return


if __name__ == '__main__':
    main()
