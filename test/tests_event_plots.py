#!/usr/bin/env python
import unittest
from os.path import join

import numpy as np
import seaborn as sns

from AS_plot.plot_utils import init_fig, savefig
from AS_plot.event import ASeventPlot
from AS_plot.settings import PLOTS_DIR


class EventPLotsTests(unittest.TestCase):
    def test_plot_elements(self):
        fig, axes = init_fig(1, 1, colsize=4, rowsize=1)
        
        event = ASeventPlot(axes=axes, pos=(-10, 20), height=4, width=20)
        
        exon1 = event.add_exon(xstart=0, width=4, alternative=False)
        exon2 = event.add_exon(xstart=10, width=4, alternative=True)
        exon3 = event.add_exon(xstart=20, width=4, alternative=False)
        exon4 = event.add_exon(xstart=30, width=4, alternative=False)
        
        event.plot_sj(exon1, exon2, loc='top')
        event.plot_sj(exon2, exon3, loc='top')
        event.plot_sj(exon1, exon3, loc='bottom')
        
        event.plot_intron(exon3, exon4)
        event.plot_intron(None, exon1)
        
        event.clean_axis()
        fpath = join(PLOTS_DIR, 'as_event.png')
        savefig(fig, fpath)
    
    def test_plot_events(self):
        fig, subplots = init_fig(5, 1, colsize=3, rowsize=0.75)
        
        event = ASeventPlot(axes=subplots[0])
        event.plot_exon_skipping()
        event.clean_axis()
        
        event = ASeventPlot(axes=subplots[1])
        event.plot_alternative_donor()
        event.clean_axis()
        
        event = ASeventPlot(axes=subplots[2])
        event.plot_alternative_acceptor()
        event.clean_axis()
        
        event = ASeventPlot(axes=subplots[3])
        event.plot_intron_retention()
        event.clean_axis()
        
        event = ASeventPlot(axes=subplots[4])
        event.plot_mutually_exclusive_exons()
        event.clean_axis()
        
        fpath = join(PLOTS_DIR, 'as_types.png')
        savefig(fig, fpath)
    
    def test_AS_event_in_plot(self):
        fig, subplots = init_fig(1, 2, colsize=3, rowsize=3)
        np.random.seed(0)
        
        # Subplot 1
        axes = subplots[0]
        x = np.random.normal(4, 2, size=1000)
        psi = np.exp(x) / (1 + np.exp(x))
        sns.distplot(psi, bins=30, hist=True, kde=False, ax=axes)
        axes.set_xlim((0, 1))
        axes.set_ylabel('Number of events')
        axes.set_xlabel(r'$\Psi$')
        event = ASeventPlot(axes=axes, pos=(0.1, 550), height=100, width=0.7)
        event.plot_exon_skipping()
        
        # Subplot 2
        axes = subplots[1]
        x = np.random.normal(2, 2, size=1000)
        psi = np.exp(x) / (1 + np.exp(x))
        sns.distplot(psi, bins=30, hist=True, kde=False, ax=axes)
        axes.set_xlim((0, 1))
        axes.set_xlabel(r'$\Psi$')
        event = ASeventPlot(axes=axes, pos=(0.1, 250), height=50, width=0.7)
        event.plot_intron_retention()
        
        sns.despine()
        fpath = join(PLOTS_DIR, 'as_plot.png')
        savefig(fig, fpath)
    

if __name__ == '__main__':
    import sys;sys.argv = ['', 'EventPLotsTests']
    unittest.main()
